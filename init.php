<?php
// Bucket Name

require './includes/aws/aws-autoloader.php';
use Aws\S3\S3Client;

$confaws = './config/configs3.ini';

$awsObj = json_decode(json_encode(parse_ini_file($confaws, true, INI_SCANNER_RAW)));
//AWS access info
$awsAccessKey = $awsObj->default->aws_access_key_id;
$awsSecretKey = $awsObj->default->aws_secret_access_key;
$bucket = $awsObj->default->aws_bucket;
//instantiate the class
$s3 = S3Client::factory(
    array(
        'credentials' => array(
            'key' => $awsAccessKey,
            'secret' => $awsSecretKey
        ),
        'version' => 'latest',
        'region'  => 'ap-southeast-2'
    )
);
?>